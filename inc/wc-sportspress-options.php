<?php


class WC_SportsPress_Sync {

	/**
	 * Adds links in WooCommerce Edit Account page.
	 */
	public static function add_links() {

		/**
		 * Load players.
		 */
		$sp_players = static::get_sp_players();
		if ( ! $sp_players ) {
			return;
		}

		/**
		 * Prepare list from players.
		 */
		$sp_players_html = '';
		foreach ($sp_players as $sp_player) {
			$sp_players_html .= sprintf( '<li><a href="%s">%s</a></li>'
				, add_query_arg( 'player', $sp_player->ID, get_option( 'wc_sportspress_form_url' ) )
				, $sp_player->post_title
			);
		}
		$links = sprintf( '<p>%s</p><p><ul>%s</ul></p>'
			, get_option( 'wc_sportspress_players_list_heading', __( 'Update Players Information', 'ca_66940' ) )
			, $sp_players_html
		);

		/**
		 * Add the links.
		 */
		static::hook_replace( 'woocommerce_edit_account_form', '<fieldset>', $links . '<fieldset>' );
	}

	/**
	 * Add a new settings tab to the WooCommerce settings tabs array.
	 *
	 * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding the Subscription tab.
	 * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including the Subscription tab.
	 */
	public static function add_settings_tab( $settings_tabs ) {
		$settings_tabs['sportspress_sync'] = __( 'SportsPress Sync', 'ca_66940' );
		return $settings_tabs;
	}

	/**
	 * Get all the settings for this plugin for @see woocommerce_admin_fields() function.
	 *
	 * @return array Array of settings for @see woocommerce_admin_fields() function.
	 */
	public static function get_settings() {
		$settings = array(
			'section_title' => array(
				'name' => __( 'WooCommerce/SportsPress Synchronization', 'ca_66940' ),
				'type' => 'title',
				'desc' => '',
				'id'   => 'wc_sportspress_sync_section_title'
			),
			'fld_0' => array(
				'name' => __( 'Form Url', 'ca_66940' ),
				'type' => 'text',
				'desc' => __( 'URL of page that contains the GravitForms form', 'ca_66940' ),
				'id'   => 'wc_sportspress_form_url'
			),
			'fld_1' => array(
				'name' => __( 'Players List Heading', 'ca_66940' ),
				'type' => 'text',
				'desc' => __( 'Text to use above the players list', 'ca_66940' ),
				'id'   => 'wc_sportspress_players_list_heading',
			),
			'section_end' => array(
				 'type' => 'sectionend',
				 'id' => 'wc_sportspress_sync_section_end'
			)
		);
		return apply_filters( 'wc_sportspress_sync_settings', $settings );
	}

	/**
	 * Get SportsPress player whose ID is $_GET[ 'player' ]
	 */
	public static function get_sp_player() {

		return ! empty( $_GET[ 'player' ] ) ? get_post( $_GET[ 'player' ] ) : false;
	}

	public static function get_sp_players() {

		$user_id = get_current_user_id();

		/**
		 * Exit function if user not logged in.
		 */
		if ( ! $user_id ) {
			return false;
		}

		/**
		 * Get and return SportsPress player if it exists.
		 */
		$sp_players = get_posts( [
			'author'         => $user_id,
			'post_type'      => 'sp_player',
			'post_status'    => 'any',
			'posts_per_page' => -1,
		] );
		if ( $sp_players ) {
			return $sp_players;
		}

		/**
		 * Player not found, return false;
		 */
		return false;
	}

	public static function hook_replace( $hook, $pattern, $replacement, $limit = 1 ) {

		add_action( $hook . '_start', function() {
			ob_start();
		} );

		add_action( $hook . '_end', function() use ( $pattern, $replacement, $limit ) {
			echo static::replace( $pattern, $replacement, ob_get_clean(), $limit );
		} );
	}

	/**
	 * Bootstraps the class and hooks required actions & filters.
	 */
	public static function init() {

		/**
		 * Add WooCommerce settings tab.
		 */
		add_filter( 'woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50 );
		add_action( 'woocommerce_settings_tabs_sportspress_sync', __CLASS__ . '::settings_tab' );
		add_action( 'woocommerce_update_options_sportspress_sync', __CLASS__ . '::update_settings' );

		/**
		 * Prepopulate GravityForms form.
		 */
		add_action( 'gform_pre_render', __CLASS__ . '::prepopulate', 10, 3 );

		/**
		 * Sync GravityForms form to SportsPress player.
		 */
		add_action( 'gform_entry_post_save', __CLASS__ . '::sync', 10, 3 );

		/**
		 * Add link to WooCommerce profile editing page.
		 */
		add_action( 'init', __CLASS__ . '::add_links' );
	}

	public static function is_sportspress( $tag, $field ) {
		return in_array( 'sportspress-' . $tag, explode( ' ', $field->cssClass) );
	}

	public static function prepopulate( $form, $ajax, $field_values ) {

		/**
		 * Get SportsPress player if it exists, exit function otherwise.
		 */
		$sp_player = static::get_sp_player();
		if ( ! $sp_player ) {
			return $form;
		}

		/**
		 * Player must belong to current user
		 */
		if ( ! self::sp_player_belongs_to_current_user( $sp_player ) ) {
			return $form;
		}

		/**
		 * Prepopulate.
		 */
		$form[ 'title' ] = str_replace( '{{player}}', $sp_player->post_title, $form[ 'title' ] );
		$sp_player_metrics = get_post_meta( $sp_player->ID, 'sp_metrics' );
		foreach ( $form[ 'fields' ] as &$field ) {
			if ( static::is_sportspress( 'bio', $field ) ) {
				$field->defaultValue = $sp_player->post_content;
			} else if ( static::is_sportspress( 'test', $field ) ) {
				$field->defaultValue = ( ! empty( $sp_player_metrics[0] ) && array_key_exists( 'test', $sp_player_metrics[0] ) )
					? $sp_player_metrics[0][ 'test']
					: ''
				;
			} else if ( static::is_sportspress( 'height', $field ) ) {
				$field->defaultValue = ( ! empty( $sp_player_metrics[0] ) && array_key_exists( 'height', $sp_player_metrics[0] ) )
					? $sp_player_metrics[0][ 'height']
					: ''
				;
			} else if ( static::is_sportspress( 'weight', $field ) ) {
				$field->defaultValue = ( ! empty( $sp_player_metrics[0] ) && array_key_exists( 'weight', $sp_player_metrics[0] ) )
					? $sp_player_metrics[0][ 'weight']
					: ''
				;
			}
		};

		return $form;
	}

	/**
	 * Replaces text using preg_replace or str_replace
	 *
	 * The function will use `preg_replace()` if `$pattern` is a valid regular expression,
	 * otherwise, it will use str_replace. Note that the function doesn't support the delimiters () and <>.
	 *
	 * @param  string  $pattern     Pattern.
	 * @param  string  $replacement Replacement string.
	 * @param  integer $limit       Limit of replacements, default is -1 for no limit.
	 * @return string
	 */
	public static function replace( $pattern, $replacement, $subject, $limit = -1 ) {

		$pattern_first_char = mb_substr( $pattern, 0, 1, 'utf-8' );
		$unsupported_delimiters = [ '(', '<' ];

		$is_regex = 1
			&& ! in_array( $pattern_first_char, $unsupported_delimiters )
			&& @ ( ! ( false === preg_match( $pattern, null ) ) )
		;

		/**
		 * Using `preg_replace()`.
		 */
		if ( $is_regex ) {
			$subject = preg_replace( $pattern, $replacement, $subject, $limit );
		}

		/**
		 * Using `str_replace()`.
		 */
		else {
			if ( $limit < 1 ) {
				$subject = str_replace( $pattern, $replacement, $subject );
			}
			while( $limit-- > 0 ) {
				$pos = strpos( $subject, $pattern );
				if ( $pos !== false ) {
					$subject = substr_replace( $subject, $replacement, $pos, strlen( $pattern ) );
				}
			}
		}

		return $subject;
	}

	/**
	 * Sets post featured image.
	 */
	public static function set_image( $player_id, $url ) {

		$path       = static::url2path( $url );
		$player     = get_post( $player_id );
		$filetype   = wp_check_filetype( basename( $path ) );
		$attachment = [
			'guid'           => $url,
			'post_mime_type' => $filetype['type'],
			'post_title'     => $player->post_title,
			'post_content'   => '',
			'post_status'    => 'publish'
		];

		// Insert the attachment.
		$attachment_id = wp_insert_attachment( $attachment, $path, $player_id );

		// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		// Generate the metadata for the attachment, and update the database record.
		$attachment_data = wp_generate_attachment_metadata( $attachment_id, $path );
		wp_update_attachment_metadata( $attachment_id, $attachment_data );

		set_post_thumbnail( $player_id, $attachment_id );
	}

	/**
	 * Set player metric.
	 */
	public static function set_metric( $player_id, $metric, $value ) {
		$metrics = get_post_meta( $player_id, 'sp_metrics' );
		if ( empty( $metrics[0] ) ) {
			$metrics[0] = [];
		}
		$metrics[0][ $metric ] = $value;
		update_post_meta( $player_id, 'sp_metrics', $metrics[0] );
	}

	/**
	 * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
	 *
	 * @uses woocommerce_admin_fields()
	 * @uses self::get_settings()
	 */
	public static function settings_tab() {
		woocommerce_admin_fields( self::get_settings() );
	}

	public function sp_player_belongs_to_current_user( $sp_player ) {
		return get_current_user_id() === ( int ) $sp_player->post_author;
	}

	/**
	 * Sync GravityForms form to SportsPress player.
	 *
	 * After entry is submitteed, the values entered will be user tp update the player.
	 */
	public static function sync( $entry, $form ) {

		/**
		 * Get SportsPress player if it exists, exit function otherwise.
		 */
		$sp_player = static::get_sp_player();
		if ( ! $sp_player ) {
			return $entry;
		}

		/**
		 * Player must belong to current user
		 */
		if ( ! self::sp_player_belongs_to_current_user( $sp_player ) ) {
			return $entry;
		}

		/**
		 * Sync.
		 */
		foreach ( $form[ 'fields' ] as $field ) {

			/**
			 * Post content.
			 */
			if ( static::is_sportspress( 'bio', $field ) ) {
				$sp_player->post_content = $entry [ $field[ 'id' ] ];
				wp_update_post( $sp_player );
			}

			/**
			 * Featured image.
			 */
			if ( static::is_sportspress( 'image', $field ) ) {
				if ( $entry[ $field[ 'id' ] ] ) {
					static::set_image( $sp_player->ID, $entry [ $field[ 'id' ] ] );
				}
			}

			/**
			 * Metric: height.
			 */
			if ( static::is_sportspress( 'height', $field ) ) {
				static::set_metric( $sp_player->ID, 'height', $entry [ $field[ 'id' ] ] );
			}

			/**
			 * Metric: weight.
			 */
			if ( static::is_sportspress( 'weight', $field ) ) {
				static::set_metric( $sp_player->ID, 'weight', $entry [ $field[ 'id' ] ] );
			}
		}

		return $entry;
	}

	/**
	 * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
	 *
	 * @uses woocommerce_update_options()
	 * @uses self::get_settings()
	 */
	public static function update_settings() {
		woocommerce_update_options( self::get_settings() );
	}

	/**
	 * Converts a url of a file in wp_content to a path.
	 */
	public static function url2path( $url ) {

		/**
		 * Not interested in files not stored in wp-content or deeper.
		 */
		$is_content_url = ( substr( $url, 0, strlen( WP_CONTENT_URL ) ) === WP_CONTENT_URL );
		if ( ! $is_content_url ) {
			return false;
		}

		/**
		 * Replace WP_CONTENT_URL with WP_CONTENT_DIR
		 */
	    return preg_replace( sprintf( '/^%s/', preg_quote( WP_CONTENT_URL, '/') ), WP_CONTENT_DIR, $url, 1 );
	}
}

WC_SportsPress_Sync::init();
