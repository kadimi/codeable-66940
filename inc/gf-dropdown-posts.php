<?php

add_filter( 'gform_pre_render'           , 'ca_66940_populate_dropdown' );
add_filter( 'gform_pre_validation'       , 'ca_66940_populate_dropdown' );
add_filter( 'gform_pre_submission_filter', 'ca_66940_populate_dropdown' );
add_filter( 'gform_admin_pre_render'     , 'ca_66940_populate_dropdown' );
add_action( 'gform_after_submission'     , 'ca_66940_update_post', 10, 2 );

function ca_66940_populate_dropdown( $form ) {

	foreach ( $form['fields'] as &$field ) {

		if ( $field->type != 'select' ) {
			continue;
		}

		/**
		 * Get and check post_type.
		 */
		$post_type = ca_66940_post_type_from_gfclass( $field->cssClass );
		if ( ! $post_type ) {
			continue;
		}

		/**
		 * Get posts of type post_type.
		 */
		$posts = get_posts( 'numberposts=-1&post_status=any&post_type=' . $post_type );

		/**
		 * Build choices.
		 */
		$choices = [];
		foreach ( $posts as $post ) {
			$choices[] = [
				'text' => $post->post_title,
				'value' => $post->ID,
			];
		}
		$field->placeholder = 'Select a Camp';
		$field->choices = $choices;
	}

	return $form;
}

function ca_66940_update_post( $entry, $form ) {


	foreach ( $form['fields'] as &$field ) {

		if ( $field->type != 'select' ) {
			continue;
		}

		/**
		 * Get and check post_type.
		 */
		$post_type = ca_66940_post_type_from_gfclass( $field->cssClass );
		if ( ! $post_type ) {
			continue;
		}

		/**
		 * Get the selected post.
		 */
		$selected_event_id = rgar( $entry, $field->id );

		/**
		 * Get newest post of type post_type.
		 */
		$newely_added_player = get_posts( 'numberposts=1&post_status=any&post_type=sp_player' )[0];

		// update_post_meta( $newely_added_player->ID , 'sp_team', $selected_event_id );
		// add_post_meta( $selected_event_id, 'sp_team', $newely_added_player->ID );
		// add_post_meta( $selected_event_id, 'sp_player', 0 );
		// add_post_meta( $selected_event_id, 'sp_player', $newely_added_player->ID );

		/**
		 * Publish player.
		 */
		wp_publish_post( $newely_added_player->ID );

		/**
		 * Add player ID to event's sp_team.
		 */
		$event_teams   = get_post_meta( $selected_event_id, 'sp_team' );
		$event_teams[] = $newely_added_player->ID;
		delete_post_meta( $selected_event_id, 'sp_team' );
		sp_update_post_meta_recursive( $selected_event_id, 'sp_team', $event_teams );

		/**
		 * Add player ID to event's sp_player.
		 */
		$event_players   = get_post_meta( $selected_event_id, 'sp_player' );
		$event_players[] = 0;
		$event_players[] = $newely_added_player->ID;
		delete_post_meta( $selected_event_id, 'sp_player' );
		sp_update_post_meta_recursive( $selected_event_id, 'sp_player', $event_players );

		// sp_update_post_meta_recursive( $newely_added_player->ID, 'sp_result', [] );
	}
}

/**
 * Get post type from prefixed post type.
 *
 * @param  string $class Class obtained from a GF field's cssClass.
 * @return string|bool   Post type of false.
 */
function ca_66940_post_type_from_gfclass( $gf_class, $prefix = 'populate-with' ) {
	$classes = explode( ' ', $gf_class );
	$regex   = '/^' . $prefix . '-/';
	/**
	 * Get, validate then return post_type.
	 */
	foreach ( $classes as $class ) {
		preg_match( $regex, $class );
		$post_type = preg_replace( $regex, '', $class );
		if ( post_type_exists( $post_type ) ) {
			return $post_type;
		}
	}
	return false;
}
